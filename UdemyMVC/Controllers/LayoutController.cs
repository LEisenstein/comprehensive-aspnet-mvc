﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate.Linq;
using UdemyMVC.Models;
using UdemyMVC.ViewModels;

namespace UdemyMVC.Controllers
{
    public class LayoutController : Controller
    {
        [ChildActionOnly]
        public ActionResult Sidebar()
        {
            //var tags = Database.Session.Query<Tag>()
            return View( new LayoutSidebar
            {
                IsLoggedIn = Auth.User != null,
                Username = Auth.User != null ? Auth.User.Username : "",
                IsAdmin = User.IsInRole("admin"),
                Tags = Database.Session.Query<Tag>().Select(t=>new
                {
                    t.Id,
                    t.Name,
                    t.Slug,
                    PostCount = t.Posts.Count
                }).Where(t=>t.PostCount > 0)
                  .OrderByDescending(t=>t.PostCount)
                  .Select(tag=> new SidebarTag(tag.Id, tag.Name, tag.Slug, tag.PostCount)).ToList()

            });
        }
    }
}