﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using NHibernate.Linq;
using UdemyMVC.Models;
using UdemyMVC.ViewModels;
namespace UdemyMVC.Controllers
{
    public class AuthController : Controller
    {
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(AuthLogin form, string returnUrl)
        {
            


            var user = Database.Session.Query<User>().FirstOrDefault(x => x.Username == form.Username);

            if(user == null)
                UdemyMVC.Models.User.FakeHash();

            if (user == null || !user.CheckPassword(form.Password))
            {
                ModelState.AddModelError("Username", "Username or password is incorrect.");
            }
                

            // Checks Data Annotations
            if (!ModelState.IsValid)
            {
                return View(form);
            }




            // Tells ASP.net the username; sets an Authentication cookie
            FormsAuthentication.SetAuthCookie(user.Username, true);

            if (!string.IsNullOrWhiteSpace(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToRoute("home");
            }

        } // Login

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToRoute("home");
        }


    }
}