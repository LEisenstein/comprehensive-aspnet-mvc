﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using NHibernate.Linq;
using UdemyMVC.Infrastructure;
using UdemyMVC.Models;
using UdemyMVC.ViewModels;

namespace UdemyMVC.Controllers
{
    public class PostsController : Controller
    {
        private const int PostsPerPage = 10;

        public ActionResult Index(int page = 1)
        {
            var baseQuery =
                Database.Session.Query<Post>().Where(x => x.DeletedAt == null).OrderByDescending(x => x.CreatedAt);

            var totalPostsCount = baseQuery.Count();
            var postIds = baseQuery.Skip((page - 1)*PostsPerPage).Take(PostsPerPage).Select(t => t.Id).ToArray();

            var posts = baseQuery.Where(p => postIds.Contains(p.Id)).FetchMany(t => t.Tags).Fetch(u => u.User).ToList();

            return View(new PostsIndex
            {
                Posts = new PagedData<Post>(posts, totalPostsCount, page, PostsPerPage)
            });
        }


        public ActionResult Tag(string idAndSlug, int page = 1)
        {
            var parts = SeparateIdAndSlug(idAndSlug);
            if (parts == null)
                return HttpNotFound();
            var tag = Database.Session.Load<Tag>(parts.Item1);

            if (tag == null)
                return HttpNotFound();


            if (!tag.Slug.Equals(parts.Item2, StringComparison.CurrentCultureIgnoreCase))
                return RedirectToRoutePermanent("tag", new {id = parts.Item1, slug = tag.Slug});


            var totalPostCount = tag.Posts.Count();
            var postIds = tag.Posts
                .OrderByDescending(g=>g.CreatedAt)
                .Where(t=>t.DeletedAt == null)
                .Skip((page - 1)*PostsPerPage)
                .Take(PostsPerPage)
                .Select(t => t.Id)
                .ToArray();

           // var baseQuery =
            //      Database.Session.Query<Post>().Where(x => x.DeletedAt == null).OrderByDescending(x => x.CreatedAt);

            var posts =
                Database.Session.Query<Post>()
                    .OrderByDescending(t => t.CreatedAt)
                    .Where(t => postIds.Contains(t.Id))
                    .FetchMany(f => f.Tags)
                    .Fetch(u => u.User)
                    .ToList();



            return View(new PostsTag
            {
                Tag = tag,
                Posts = new PagedData<Post>(posts, totalPostCount, page, PostsPerPage)
            });



        }

        public ActionResult Show(string idAndSlug)
        {
            var parts = SeparateIdAndSlug(idAndSlug);
            if (parts == null)
                return HttpNotFound();
            var post = Database.Session.Load<Post>(parts.Item1);

            if (post == null || post.IsDeleted)
                return HttpNotFound();

            // if the ID is correct, but the Slug is not; Redirect to the correct id/slug combination
            // these weird routes and handling are for SEO purposes
            if (!post.Slug.Equals(parts.Item2, StringComparison.CurrentCultureIgnoreCase))
                return RedirectToRoutePermanent("Post", new {id = parts.Item1, slug = post.Slug});

            return View(new PostsShow
            {
                Post = post
            });
        }

        private Tuple<int, string> SeparateIdAndSlug(string idAndSlug)
        {
            var matches  = Regex.Match(idAndSlug, @"^(\d+)\-(.*)?$");
            if (!matches.Success)
                return null;

            var id = int.Parse(matches.Result("$1"));
            var slug = matches.Result("$2");


            var retTuple = Tuple.Create(id, slug);
            return retTuple;
        }
    }
}