﻿using System;
using System.Web.Mvc;

namespace UdemyMVC.Infrastructure
{
    public class TransactionFilter : IActionFilter
    {
        // Global ActionFilter that occurs every time an action is started
        // This is not an attribute.  It is registered in the Global.asax, so it auto applies to All Controller/Actions


        // This sets up Transactions for NHibernate by Creating a Transaction as soon as any Action is Executed
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Database.Session.BeginTransaction();
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if(filterContext.Exception == null)
                Database.Session.Transaction.Commit();
            else
                Database.Session.Transaction.Rollback();
        }

      
    }
}