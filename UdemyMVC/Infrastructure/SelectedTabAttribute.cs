﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
namespace UdemyMVC.Infrastructure
{
    // This attribute will take a string to determine which tab should be active
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class  SelectedTabAttribute : ActionFilterAttribute
    {
        private readonly string _selectedTab ;
        public SelectedTabAttribute(string selectedTab)
        {
            _selectedTab = selectedTab;
        }

        // Alt+Insert
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            filterContext.Controller.ViewBag.SelectedTab = _selectedTab;
        }
    }
}
