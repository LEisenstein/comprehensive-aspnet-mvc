﻿using System.Data;
using FluentMigrator;
using FluentMigrator.Expressions;

namespace UdemyMVC.Migrations
{
    [Migration(1)]
    public class _001_UsersAndRoles : Migration
    {
        public override void Up()
        {
            Create.Table("users")
                .WithColumn("id").AsInt32().Identity().PrimaryKey()
                .WithColumn("username").AsString(128)
                .WithColumn("email").AsCustom("VARCHAR(256)")
                .WithColumn("password_hash").AsString(128);

            Create.Table("roles")
                .WithColumn("id").AsInt32().Identity().PrimaryKey()
                .WithColumn("name").AsString(128);
            Create.Table("role_users")
                .WithColumn("user_id").AsInt32().ForeignKey("users", "id").OnDelete(Rule.Cascade)  // When User is deleted, delete the user_id foreign keys
                .WithColumn("role_id").AsInt32().ForeignKey("roles", "id").OnDelete(Rule.Cascade); // When Role is deleted, delete the role_id foreign keys

        }

        public override void Down()
        {
            Delete.Table("role_users");  // Must be deleted before users/roles
            Delete.Table("users");
            Delete.Table("roles");

        }
    }
}