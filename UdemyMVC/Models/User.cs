﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace UdemyMVC.Models
{
    public class User
    {
        private const int WorkFactor = 13;
        // ALL Members method/property MUST BE VIRTUAL
        // Virtual lets NHibernation override them
        public virtual int Id { get; set; }
        public virtual string Username { get; set; }       
        public virtual string Email { get; set; }
        public virtual string PasswordHash { get; set; }

        public virtual IList<Role> Roles { get; set; }


        public User()
        {
            Roles = new List<Role>();
        }


        public virtual void SetPassword(string password)
        {
            PasswordHash = BCrypt.Net.BCrypt.HashPassword(password, WorkFactor);
        }

        public virtual bool CheckPassword(string password)
        {
            return BCrypt.Net.BCrypt.Verify(password, PasswordHash);
        }

        public static void FakeHash()
        {
            // This fake hash is to simulate a password check so hackers can't do timing attacks
            BCrypt.Net.BCrypt.HashPassword("", WorkFactor);
        }
    }


    // Mappings; code based mapping
    public class UserMap : ClassMapping<User>
    {
        public UserMap()
        {
            Table("users");

            // tells PK and Auto Increment Identity
            // (Expression<Func<User, int>>, Action<IIdMapper>)
            Id(x=>x.Id, x=>x.Generator(Generators.Identity));


            // Tell NHibernate about fields
            Property(x=>x.Username, x=>x.NotNullable(true));
            Property(x=>x.Email, x=>x.NotNullable(true));
            Property(x=>x.PasswordHash, x =>
            {
                x.Column("password_hash");
                x.NotNullable(true);
            });

            // tell nH the collection(roles), the pivot table role_users, id of user, id of role
            Bag(x => x.Roles, x =>
            {
                x.Table("role_users");
                x.Key(k=>k.Column("user_id"));

            }, x => x.ManyToMany(k=>k.Column("role_id")));
        }
    }


}