﻿using System.Collections.Generic;
using NHibernate.Mapping;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace UdemyMVC.Models
{
    public class Tag
    {
        public virtual int Id { get; set; }
        public virtual string Slug { get; set; }
        public virtual string Name { get; set; }


        // Many to Many with Posts
        public virtual IList<Post> Posts { get; set; }

        public Tag()
        {
            Posts = new List<Post>();
        } 
    }


    public class TagMap : ClassMapping<Tag>
    {
        public TagMap()
        {
            Table("tags");
            Id(x => x.Id, x => x.Generator(Generators.Identity));
            Property(x=>x.Slug, x=>x.NotNullable(true));
            Property(x=>x.Name, x=>x.NotNullable(true));

            
            // Many to Many Mapping is done with a Bag
            // Pivot table will be used; 1 tag_id, many post_id & vice versa
            Bag(x=>x.Posts, x =>
            {
                x.Key(y=>y.Column("tag_id"));
                x.Table("posts_tags");

            }, x=>x.ManyToMany(y=>y.Column("posts_id")));
        }
    }
}