﻿using System;
using System.Collections.Generic;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;

namespace UdemyMVC.Models
{
    public class Post
    {
        public Post()
        {
            Tags = new List<Tag>();
        }
        public virtual int Id { get; set; }
        public virtual User User { get; set; }

        public virtual string Title { get; set; }
        public virtual string Slug { get; set; }
        public virtual string Content { get; set; }

        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime? UpdatedAt { get; set; }
        public virtual DateTime? DeletedAt { get; set; }


        public virtual IList<Tag> Tags { get; set; } 
        public virtual bool IsDeleted
        {
            get { return DeletedAt != null; }
        }
    }




    public class PostMap : ClassMapping<Post>
    {
        public PostMap()
        {
            Table("posts");
            Id(x => x.Id, x => x.Generator(Generators.Identity));
            
            
            // Many to One association between Post and User
            ManyToOne(x=>x.User, x =>
            {
                x.Column("user_id");
                x.NotNullable(true);
            });


            Property(x=>x.Title, x=>x.NotNullable(true));
            Property(x=>x.Slug, x=>x.NotNullable(true));
            Property(x=>x.Content, x=>x.NotNullable(true));

            Property(x=>x.CreatedAt, x =>
            {
                x.Column("created_at");
                x.NotNullable(true);
            });
            Property(x=>x.UpdatedAt, x=>x.Column("updated_at"));
            Property(x=>x.DeletedAt, x=>x.Column("deleted_at"));


            // Many to Many Mapping is done with a Bag
            // Pivot table will be used; 1 posts_id, many tag_id & vice versa
            Bag(x => x.Tags, x =>
            {
                x.Key(y => y.Column("posts_id"));
                x.Table("posts_tags");

            }, x => x.ManyToMany(y => y.Column("tag_id")));
        }
    }







}