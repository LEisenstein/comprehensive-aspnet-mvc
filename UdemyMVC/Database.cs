﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping.ByCode;
using UdemyMVC.Models;

namespace UdemyMVC
{
    // Loaded via Global.asax
    public static class Database
    {
        private const string SessionKey = "UdemyMVC.Database.SessionKey";
        private static ISessionFactory _sessionFactory;

        public static ISession Session
        {
            get
            {
                return (ISession) HttpContext.Current.Items[SessionKey]; 

            }
          
        }
        public static void Configure()
        {
            var config = new Configuration();

            // connectionString
            // looks at special section in Web.config; must be followed EXACTLY
            config.Configure(); 

            //mappings
            var mapper = new ModelMapper();
            mapper.AddMapping<UserMap>();
            mapper.AddMapping<RoleMap>();
            mapper.AddMapping<PostMap>();
            mapper.AddMapping<TagMap>();
            

            config.AddMapping(mapper.CompileMappingForAllExplicitlyAddedEntities());
            

            //create sessionfactory
            _sessionFactory = config.BuildSessionFactory();
        }


        public static void OpenSession()
        {
            // HttpContext is only available 1/request; this looks at current request
            // Makes session available for each request.  A connection to the database
            HttpContext.Current.Items[SessionKey] = _sessionFactory.OpenSession();
        }

        public static void CloseSession()
        {
            var session = HttpContext.Current.Items[SessionKey] as ISession;

            if (session != null)
                session.Close();

            HttpContext.Current.Items.Remove(SessionKey);
        }
    }
}
