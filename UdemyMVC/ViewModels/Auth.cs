﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// 1 class with all the ViewModels in it
// Should have 1 class per action
namespace UdemyMVC.ViewModels
{
    public class AuthLogin
    {
        [Required]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }


    }
}
