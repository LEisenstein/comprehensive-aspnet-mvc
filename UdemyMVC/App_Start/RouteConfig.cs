﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using UdemyMVC.Controllers;

namespace UdemyMVC
{
    // This deals with the main Controllers folder, not the Areas Controller folder
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            // set the default namespace for the Controllers so we don't accidentally call the /admin Area controllers
            var namespaces = new[] {typeof(UdemyMVC.Controllers.PostsController).Namespace};
          // special extension for files that don't exist on the file system but they are used for debugging
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");




            routes.MapRoute("TagForRealThisTime", "tag/{idAndSlug}", new {controller = "Posts", action = "Tag"}, namespaces);
            // We are going to write our own routes and not use the default route
            // We will define every route for the front end
            routes.MapRoute("Tag", "tag/{id}-{slug}", new { controller = "Posts", action = "Tag" }, namespaces);


            // http://blog.dev/post/432-this-is-a-slug
            routes.MapRoute("PostForRealThisTime", "post/{idAndSlug}", new {controller = "Posts", action = "Show"}, namespaces);
            routes.MapRoute("Post", "post/{id}-{slug}", new {controller = "Posts", action = "Show"}, namespaces);

            //   /login will redirect to auth controller, Login action
            routes.MapRoute("Login", "login", new { controller = "Auth", action = "Login"}, namespaces);

            //   /logout  will redirect to auth controler, Logout action
            routes.MapRoute("Logout", "logout", new {controller = "Auth", action = "logout"}, namespaces);


            

            //     /    will redirect to Posts controller, Index action
            routes.MapRoute("Home", "", new { controller = "Posts", action = "Index"}, namespaces);
           

            routes.MapRoute("Sidebar", "", new {controller = "Layout", action = "Sidebar"}, namespaces);




            // Error Routes
            routes.MapRoute("Error500", "errors/500", new {controller = "Errors", action = "Error"}, namespaces);
            routes.MapRoute("Error404", "errors/404", new { controller = "Errors", action = "NotFound" }, namespaces);
        }
    }
}
