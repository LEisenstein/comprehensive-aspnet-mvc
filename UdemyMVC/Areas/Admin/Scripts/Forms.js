﻿$(document).ready(function() {
    $("a[data-post]").click(function(e) {
        e.preventDefault();  // prevent link navigation

        var $this = $(this);
        var message = $this.data("post");

        if (message && !confirm(message)) {
            return;
        }
        
        var antiForgeryToken = $("#anti-forgery-form input");
        var antiForgeryInput = $("<input type='hidden'>").attr("name", antiForgeryToken.attr("name")).val(antiForgeryToken.val());
        

        // if data-post if the confirm message, Submit the form method POST
        $("<form>")
            .attr("method", "POST")
            .attr("action", $this.attr("href"))
            .append(antiForgeryInput)
            .appendTo(document.body)
            .submit();
    });

    // foreach data-slug container
    $("[data-slug]").each(function() {
        var $this = $(this);
        var $sendSlugFrom = $($this.data("slug"));  // get the text field

        // event to watch key preses
        $sendSlugFrom.keyup(function() {
            var slug = $sendSlugFrom.val();
            slug = slug.replace(/[^a-zA-Z0-9\s]/g, "");
            slug = slug.toLowerCase();
            slug = slug.replace(/\s+/g, "-");

            if (slug.charAt(slug.length - 1) == "-")
                slug = slug.substr(0, slug.length - 1);

            // update our value to equal the slug
            $this.val(slug);
        });
    });


});