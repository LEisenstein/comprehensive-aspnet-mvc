﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UdemyMVC.Infrastructure;
using UdemyMVC.Models;

namespace UdemyMVC.Areas.Admin.ViewModels
{
    public class PostsIndex
    {
         public PagedData<Post> Posts { get; set; }
         
    }

    public class PostsForm
    {
        public PostsForm()
        {
            Tags = new List<TagCheckbox>();
        }
        public bool IsNew { get; set; }
        
        public int? PostId { get; set; }

        [Required, MaxLength(128)]
        public string Title { get; set; }
        
        [Required, DataType(DataType.MultilineText)]
        public string Content { get; set; }

        [Required, MaxLength(128)]
        public string Slug { get; set; }

        public IList<TagCheckbox> Tags { get; set; }
    }



    public class TagCheckbox
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public bool IsChecked { get; set; }

    }



















}