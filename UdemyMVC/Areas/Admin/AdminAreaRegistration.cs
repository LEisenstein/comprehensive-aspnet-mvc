﻿using System.Web.Mvc;

namespace UdemyMVC.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        // Areas are Automatically discovered by ASP.NET
        public override string AreaName { get { return "admin"; } }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            // register routes specific to this Area    
            context.MapRoute(
                "Admin_default",
                "admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}